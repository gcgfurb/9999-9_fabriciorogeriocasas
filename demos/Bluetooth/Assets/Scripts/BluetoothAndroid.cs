﻿using System.Text;
using UnityEngine;

public class BluetoothAndroid
{
    public string[] Address { get; set; }

    GameObject go;

    public BluetoothAndroid(GameObject go)
    {
        this.go = go;
    }

    const string JAVA_CLASS_NAME = "com.furb.tecedu.unitypluginbluetooth.Service";

    public void CreateServiceClient()
    {
        using (AndroidJavaClass plugin = new AndroidJavaClass(JAVA_CLASS_NAME))
        {
            Debug.Log("createServiceClient");
            plugin.CallStatic("createServiceClient", go.name);

        }
    }

    public void CreateServiceServer()
    {
        using (AndroidJavaClass plugin = new AndroidJavaClass(JAVA_CLASS_NAME))
        {
            Debug.Log("createServiceServer");
            plugin.CallStatic("createServiceServer", go.name);
        }
    }

    public void StartService(string pairingAddress)
    {
        Debug.Log("Connect: " + pairingAddress);
        using (AndroidJavaClass plugin = new AndroidJavaClass(JAVA_CLASS_NAME))
        {
            plugin.CallStatic("start", pairingAddress);
        }
    }

    public void StopService()
    {
        using (AndroidJavaClass plugin = new AndroidJavaClass(JAVA_CLASS_NAME))
        {
            plugin.CallStatic("stop");
        }
    }

    public void Write(byte[] data, int length, bool withResponse)
    {
        using (AndroidJavaClass plugin = new AndroidJavaClass(JAVA_CLASS_NAME))
        {
            Debug.Log("Write - msg: " + Encoding.UTF8.GetString(data));
            plugin.CallStatic("write", data);
        }
    }

    public void GetBoundDevices()
    {
        using (AndroidJavaClass plugin = new AndroidJavaClass(JAVA_CLASS_NAME))
        {
            Address = plugin.CallStatic<string[]>("getBondedDevices");

            foreach (string a in Address)
            {
                Debug.Log("Address: " + a);
            }
        }
    }
}
