﻿using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class BluetoothUI : MonoBehaviour
{
    public Text address;
    BluetoothAndroid bluetooth;
    CallbackAndroid callback;

    public GameObject go;

    // Use this for initialization
    void Start()
    {
        callback = go.GetComponent<CallbackAndroid>();
        bluetooth = new BluetoothAndroid(go);
    }

    public void BluetoothConnectServer()
    {
        bluetooth.CreateServiceServer();
    }

    public void BluetoothConnectClient()
    {
        bluetooth.CreateServiceClient();
    }
    public void Devices()
    {
        bluetooth.GetBoundDevices();
        address.text = Regex.Split(bluetooth.Address[0], @"<>").GetValue(1).ToString();
    }

    public void Connect()
    {
        bluetooth.StartService(address.text);
    }

    public void Disconnection()
    {
        bluetooth.StopService();
    }

    public void Disconnect()
    {
        bluetooth.StopService();
    }

    public void Write()
    {
        bluetooth.Write(Encoding.ASCII.GetBytes("Envio mensagem"), 14, false);
    }
}
