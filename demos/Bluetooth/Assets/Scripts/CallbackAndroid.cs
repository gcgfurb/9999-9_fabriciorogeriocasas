﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class CallbackAndroid : MonoBehaviour {

    public Text callback;

    public void OnDidConnect()
    {
        callback.text = "OnDidConnect";
        Debug.Log(callback.text);
    }

    public void OnDidDisconnect()
    {
        callback.text = "OnDidDisconnect";
        Debug.Log(callback.text);
    }

    public void OnDidReceiveWriteRequests(string base64String)
    {
        byte[] data = Convert.FromBase64String(base64String);
        string decodedString = Encoding.UTF8.GetString(data);
        callback.text = "OnDidReceiveWriteRequests base: " + decodedString;
        Debug.Log(callback.text);
    }

    public void OnDidUpdateState()
    {
        callback.text = "OnDidUpdateState";
        Debug.Log(callback.text);
    }
}
