﻿using UnityEngine;

public class BluetoothAndroid : MonoBehaviour
{
    public string[] Address { get; set; }

    GameObject callback;

    public void SetGameObjectCallback(GameObject callback)
    {
        this.callback = callback;
    }

    const string JAVA_CLASS_NAME = "com.furb.tecedu.unitypluginbluetooth.Service";

    public void CreateServiceClient()
    {
        using (AndroidJavaClass plugin = new AndroidJavaClass(JAVA_CLASS_NAME))
        {
            plugin.CallStatic("createServiceClient", callback.name);
        }
    }

    public void CreateServiceServer()
    {
        using (AndroidJavaClass plugin = new AndroidJavaClass(JAVA_CLASS_NAME))
        {
            plugin.CallStatic("createServiceServer", callback.name);
        }
    }

    public void StartService(string pairingAddress)
    {
        using (AndroidJavaClass plugin = new AndroidJavaClass(JAVA_CLASS_NAME))
        {
            plugin.CallStatic("start", pairingAddress);
        }
    }

    public void StopService()
    {
        using (AndroidJavaClass plugin = new AndroidJavaClass(JAVA_CLASS_NAME))
        {
            plugin.CallStatic("stop");
        }
    }

    public void Write(byte[] data, int length, bool withResponse)
    {
        using (AndroidJavaClass plugin = new AndroidJavaClass(JAVA_CLASS_NAME))
        {
            plugin.CallStatic("write", data);
        }
    }

    public void GetBoundDevices()
    {
        using (AndroidJavaClass plugin = new AndroidJavaClass(JAVA_CLASS_NAME))
        {
            Address = plugin.CallStatic<string[]>("getBondedDevices");
        }
    }
}
