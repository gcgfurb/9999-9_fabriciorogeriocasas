﻿using System.Text;
using UnityEngine;

public class BluetoothManager : MonoBehaviour {
    
    public BluetoothAndroid bluetooth;
    public bool isServer;
    
	// Use this for initialization
	void Start () {
        bluetooth.SetGameObjectCallback(transform.gameObject);
        DontDestroyOnLoad(transform.gameObject);	
	}

    public void Server()
    {
        bluetooth.CreateServiceServer();
    }

    public void Client()
    {
        bluetooth.CreateServiceClient();
        bluetooth.GetBoundDevices();
    }

    public string[] Devices()
    {
        bluetooth.GetBoundDevices();
        return bluetooth.Address;
    }

    public void Connect(string address)
    {
        bluetooth.StartService(address);
    }

    public void Disconnect()
    {
        bluetooth.StopService();
    }

    public void SendStatus(string message)
    {
        byte[] data = Encoding.ASCII.GetBytes(message.ToCharArray());
        bluetooth.Write(data, data.Length, false);
    }
}
