﻿using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BluetoothUI : MonoBehaviour {
    
    public GameObject addressButton;
    public GameObject panelDevices;
    private BluetoothManager bluetoothManager;

    private void Start()
    {
        GameObject obj = GameObject.Find("Monitor").gameObject;
        bluetoothManager = (BluetoothManager)obj.GetComponent("BluetoothManager");
    }

    public void BtServer()
    {
        bluetoothManager.isServer = true;
        bluetoothManager.Server();
        bluetoothManager.Connect("server");
    }

    public void BtClient()
    {
        bluetoothManager.isServer = false;
        bluetoothManager.Client();
        CarregarCanvasDevices();
    }

    public void BtVoltar()
    {
        bluetoothManager.Disconnect();
        SceneManager.LoadScene("SelectMultiplayer");
    }

    public void Connect(string address)
    {
        bluetoothManager.Connect(address);
    }

    public void CarregarCanvasDevices()
    {
        string[] listAddress = bluetoothManager.Devices();
        foreach(string address in listAddress)
        {
            string[] infoAddress = Regex.Split(address, @"<>");

            GameObject newButton = Instantiate(addressButton);
            newButton.GetComponentInChildren<Text>().text = infoAddress.GetValue(0).ToString();
            newButton.transform.SetParent(panelDevices.transform, false);
            
            newButton.GetComponent<Button>().onClick.AddListener(() => {
                this.Connect(infoAddress.GetValue(1).ToString());
            });
        }
    }
}
