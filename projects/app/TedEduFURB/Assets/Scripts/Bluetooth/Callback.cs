﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Callback : MonoBehaviour {

    public Status Status { get; set; }
    private void Awake()
    {
        Status = new Status();    
    }

    public void OnDidConnect()
    {
        SceneManager.LoadScene("MultiplayerBluetooth");
    }

    public void OnDidDisconnect()
    {
        // TODO apresentar mensagem que não conseguiu conectar ao servidor
        SceneManager.LoadScene("SelectMultiplayer");
    }

    public void OnDidReceiveWriteRequests(string base64String)
    {
        byte[] data = Convert.FromBase64String(base64String);
        string json = Encoding.UTF8.GetString(data);

        //Status status = new Status();
        //Status.SetStatusFromJson(json);
        Status = JsonUtility.FromJson<Status>(json);
    }

    public void OnDidUpdateState()
    {
    }
}
