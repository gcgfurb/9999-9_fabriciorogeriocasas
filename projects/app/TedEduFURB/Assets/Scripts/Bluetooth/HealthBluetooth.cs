﻿using UnityEngine;

public class HealthBluetooth : MonoBehaviour
{
    public HealthbarBluetooth healthbar;

    #region GASOLINE
    private const float maxGasolineHealth = 100f;
    private float gasolineCurrent = maxGasolineHealth;

    public float GasolineCurrent
    {
        get
        {
            return gasolineCurrent;
        }
        set
        {
            gasolineCurrent = value;
            healthbar.OnChangeGasoline(gasolineCurrent);
            TakeDamageGasoline();
        }
    }

    public void TakeDamageGasoline(float gasoline)
    {
        GasolineCurrent = GasolineCurrent += gasoline;
    }

    private void TakeDamageGasoline()
    {
        if (gasolineCurrent <= 0)
            DestroyObject(gameObject);
    }
    #endregion

    #region MAINTENANCE
    private const float maxMaintenanceHealth = 100f;
    private float maintenanceCurrent = maxMaintenanceHealth;

    public float MaintenanceCurrent
    {
        get
        {
            return maintenanceCurrent;
        }
        set
        {
            maintenanceCurrent = value;
            healthbar.OnChangeMaintenance(maintenanceCurrent);
            TakeDamageMaintenance();
        }
    }

    public void TakeDamageMaintenance(float maintenance)
    {
        MaintenanceCurrent = MaintenanceCurrent + maintenance;
    }

    private void TakeDamageMaintenance()
    {
        if (maintenanceCurrent <= 0)
            DestroyObject(gameObject);
    }
    #endregion
}
