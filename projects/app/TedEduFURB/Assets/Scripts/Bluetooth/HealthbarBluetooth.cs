﻿using UnityEngine;

public class HealthbarBluetooth :MonoBehaviour
{
    #region GASOLINE
    public RectTransform gasolineHealthbar;

    public void OnChangeGasoline(float gasoline)
    {
        gasolineHealthbar.sizeDelta = new Vector2(gasoline, gasolineHealthbar.sizeDelta.y);
    }
    #endregion

    #region MAINTENANCE
    public RectTransform maintenanceHealthbar;

    public void OnChangeMaintenance(float maintenance)
    {
        maintenanceHealthbar.sizeDelta = new Vector2(maintenance, maintenanceHealthbar.sizeDelta.y);
    }
    #endregion
}
