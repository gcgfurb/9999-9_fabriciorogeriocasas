﻿using System.Collections;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerBluetooth : MonoBehaviour
{
    public GameObject opponentCar;
    public HealthBluetooth health;

    private BluetoothManager bluetoothManager;
    private Callback callback;
    private PlayerOpponentBluetooth opponent;

    private float damageHouse = -3.0f;
    private float damageMechanics = -3.0f;
    private float damageGasStation = -3.0f;
    private float damageCar = -3.0f;
    private float damageHole = -2.0f;

    private bool waitRepair = false;
    private float repairForSecond = 2.0f;

    private bool waitFuel = false;
    private float fuelForSecond = 10.0f;

    private float speedRotation = 40.0f;
    private float speedTranslation = 50.0f;

    private float consumptionRotation = -0.1f;
    private float consumptionTranslation = -0.1f;

    private void Awake()
    {
        GameObject monitor = GameObject.Find("Monitor").gameObject;

        bluetoothManager = (BluetoothManager)monitor.GetComponent("BluetoothManager");

        callback = (Callback)monitor.GetComponent("Callback");

        opponent = (PlayerOpponentBluetooth)opponentCar.GetComponent("PlayerOpponentBluetooth");

        GameObject spawnServer = GameObject.Find("SpawnPointServer");
        GameObject spawnClient = GameObject.Find("SpawnPointClient");

        if (bluetoothManager.isServer)
        {
            transform.localPosition = spawnServer.transform.localPosition;
            opponent.transform.localPosition = spawnClient.transform.localPosition;
        }
        else
        {
            transform.localPosition = spawnClient.transform.localPosition;
            opponent.transform.localPosition = spawnServer.transform.localPosition;
        }
    }

    // Use this for initialization
    void Start()
    {
        // Inicializa a posição e as variaveis do oponente
        callback.Status.GasolineCurrent = 100f;
        callback.Status.MaintenanceCurrent = 100f;
        callback.Status.PositionX = opponent.transform.localPosition.x;
        callback.Status.PositionY = opponent.transform.localPosition.y;
        callback.Status.PositionZ = opponent.transform.localPosition.z;
    }

    // Update is called once per frame
    void Update()
    {
        UpdatePlayer();
        UpdateOppinentPlayer();
        SendPlayerToOpponent();
    }

    private void UpdatePlayer()
    {
        float horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
        float vertical = CrossPlatformInputManager.GetAxis("Vertical");

        if (horizontal != 0 && (horizontal > 0.30f || horizontal < -0.30f))
        {
            float rotation = (horizontal * speedRotation) * Time.deltaTime;
            if (horizontal < 0 && vertical < 0 || horizontal > 0 && vertical < 0f)
                transform.Rotate(0, rotation * -1, 0);
            else
                transform.Rotate(0, rotation, 0);

            TakeDamageMoviment(consumptionRotation);
        }

        if (vertical != 0)
        {
            float translation = (vertical * speedTranslation) * Time.deltaTime;
            transform.Translate(0, 0, translation);

            TakeDamageMoviment(consumptionTranslation);
        }

        if (horizontal != 0 || vertical != 0)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, 0.0087f, transform.localPosition.z);
        }
    }

    private void UpdateOppinentPlayer()
    {
        Status status = callback.Status;
        opponent.Gasoline = status.GasolineCurrent;
        opponent.Maintenance = status.MaintenanceCurrent;

        opponent.Position = new Vector3(status.PositionX, status.PositionY, status.PositionZ);
    }

    private void SendPlayerToOpponent()
    {
        Status status = new Status()
        {
            GasolineCurrent = health.GasolineCurrent,
            MaintenanceCurrent = health.MaintenanceCurrent,
            PositionX = transform.localPosition.x,
            PositionY = transform.localPosition.y,
            PositionZ = transform.localPosition.z
        };
        
        bluetoothManager.SendStatus(status.GetJsonToStatus());
    }

    void TakeDamageMoviment(float damage)
    {
        health.TakeDamageGasoline(damage);
    }

    void OnDestroy()
    {
        GameObject joy = GameObject.FindGameObjectWithTag("JoystickControl");
        if (joy != null)
        {
            Destroy(joy);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Casa":
                health.TakeDamageMaintenance(damageHouse);
                break;
            case "Mecanica":
                health.TakeDamageMaintenance(damageMechanics);
                break;
            case "PostoGasolina":
                health.TakeDamageMaintenance(damageGasStation);
                break;
            case "CarroVermelho":
                health.TakeDamageMaintenance(damageCar);
                break;
        }
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Buraco")
        {
            health.TakeDamageMaintenance(damageHole);
        }
    }

    void OnTriggerStay(Collider collision)
    {
        switch (collision.gameObject.tag)
        {
            case "ChaveInglesa":
                if (!waitRepair)
                {
                    StartCoroutine(RepairMaintenance());
                }
                break;
            case "GalaoDeGasolina":
                if (!waitFuel)
                {
                    StartCoroutine(FuelGasoline());
                }
                break;
        }
    }

    private IEnumerator FuelGasoline()
    {
        health.TakeDamageGasoline(fuelForSecond);
        waitFuel = true;
        yield return new WaitForSeconds(1);
        waitFuel = false;
    }

    private IEnumerator RepairMaintenance()
    {
        health.TakeDamageMaintenance(repairForSecond);
        waitRepair = true;
        yield return new WaitForSeconds(1);
        waitRepair = false;
    }
}