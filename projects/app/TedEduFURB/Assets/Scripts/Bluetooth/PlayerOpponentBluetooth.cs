﻿using UnityEngine;

public class PlayerOpponentBluetooth : MonoBehaviour
{
    private float speedRotation = 40.0f;
    private float speedTranslation = 50.0f;

    public float Gasoline { get; set; }
    public float Maintenance { get; set; }

    public float VerticalPosition { get; set; }
    public float HorizontalPosition { get; set; }
    public Vector3 Position { get; set; }

    public HealthBluetooth health;

    void Update()
    {
        UpdatePosition();
        UpdateHealth();        
    }

    private void UpdatePosition()
    {
        transform.localPosition = Position;
    }
        
    private void UpdateHealth()
    {
        health.GasolineCurrent = Gasoline;
        health.MaintenanceCurrent = Maintenance;
    }
}
