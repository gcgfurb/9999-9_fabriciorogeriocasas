﻿using UnityEngine;

public class Status
{
    public float gasolineCurrent;
    public float GasolineCurrent
    {
        get
        {
            return gasolineCurrent;
        }
        set
        {
            gasolineCurrent = value;
        }
    }

    public float maintenanceCurrent;
    public float MaintenanceCurrent
    {
        get
        {
            return maintenanceCurrent;
        }
        set
        {
            maintenanceCurrent = value;
        }
    }

    public float positionX;
    public float PositionX
    {
        get
        {
            return positionX;
        }
        set
        {
            positionX = value;
        }
    }

    public float positionY;
    public float PositionY
    {
        get
        {
            return positionY;
        }
        set
        {
            positionY = value;
        }
    }

    public float positionZ;
    public float PositionZ
    {
        get
        {
            return positionZ;
        }
        set
        {
            positionZ = value;
        }
    }

    public string GetJsonToStatus()
    {
        return JsonUtility.ToJson(this);
    }
}
