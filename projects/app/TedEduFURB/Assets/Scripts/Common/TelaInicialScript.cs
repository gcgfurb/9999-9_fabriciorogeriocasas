﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class TelaInicialScript : MonoBehaviour
{
    public void VoltarTelaInicial()
    {
        SceneManager.LoadScene("TelaInicial");
    }

    public void Sobre()
    {
        Application.OpenURL("http://www.inf.furb.br/gcg/tecedu/transito");
    }

    public void EducacaoNoTransito()
    {
        Application.OpenURL("http://www.inf.furb.br/gcg/tecedu/transito/educa.pdf");
    }

    public void Imprimir()
    {
        Application.OpenURL("http://www.inf.furb.br/gcg/tecedu/transito/mapa.pdf");
    }

    public void Iniciar()
    {
        SceneManager.LoadScene("ControlePorJoystick");
    }

    public void Multiplayer()
    {
        //SceneManager.LoadScene("Lobby");
        SceneManager.LoadScene("SelectMultiplayer");
    }
}
