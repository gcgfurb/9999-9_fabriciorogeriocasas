﻿using UnityEngine;
using UnityEngine.Networking;

public class Health : NetworkBehaviour {

    public const float maxGasolineHealth = 100f;
    [SyncVar(hook = "OnChangeGasoline")]
    public float gasolineCurrent = maxGasolineHealth;
    public RectTransform gasolineHealthbar;

    public const float maxMaintenanceHealth = 100f;
    [SyncVar(hook = "OnChangeMaintenance")]
    public float maintenanceCurrent = maxMaintenanceHealth;
    public RectTransform maintenanceHealthbar;
    
    public void TakeDamageGasoline(float gasoline)
    {
        if (!isServer)
            return;

        gasolineCurrent += gasoline;

        if (gasolineCurrent <= 0)
            DestroyObject(gameObject);
    }

    void OnChangeGasoline(float gasoline)
    {
        gasolineHealthbar.sizeDelta = new Vector2(gasoline, gasolineHealthbar.sizeDelta.y);
    }

    public void TakeDamageMaintenance(float maintenance)
    {
        if (!isServer)
            return;

        maintenanceCurrent += maintenance;

        if (maintenanceCurrent <= 0)
            DestroyObject(gameObject);
    }

    void OnChangeMaintenance(float maintenance)
    {
        maintenanceHealthbar.sizeDelta = new Vector2(maintenance, maintenanceHealthbar.sizeDelta.y);
    }
}
