﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.CrossPlatformInput;

public class Player : NetworkBehaviour {

    public Health health;

    [SyncVar]
    public Color color;

    private float damageHouse = -3.0f;
    private float damageMechanics = -3.0f;
    private float damageGasStation = -3.0f;
    private float damageHole = -2.0f;

    private bool waitRepair = false;
    private float repairForSecond = 2.0f;

    private bool waitFuel = false;
    private float fuelForSecond = 10.0f;

    private float speedRotation = 40.0f;
    private float speedTranslation = 50.0f;

    private float consumptionRotation = -0.1f;
    private float consumptionTranslation = -0.1f;

    private void Start()
    {
        Renderer[] rends = GetComponentsInChildren<Renderer>();
        foreach (Renderer r in rends)
        {
            r.material.color = color;
        }
    }

    void Update ()
    {
        if (!isLocalPlayer)
            return;

        float horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
        float vertical = CrossPlatformInputManager.GetAxis("Vertical");

        if (horizontal != 0 && (horizontal > 0.30f || horizontal < -0.30f))
        {
            float rotation = (horizontal * speedRotation) * Time.deltaTime;
            if (horizontal < 0 && vertical < 0 || horizontal > 0 && vertical < 0f)
                transform.Rotate(0, rotation * -1, 0);
            else
                transform.Rotate(0, rotation, 0);

            CmdTakeDamageMoviment(consumptionRotation);
        }

        if (vertical != 0)
        {
            float translation = (vertical * speedTranslation) * Time.deltaTime;
            transform.Translate(0, 0, translation);

            CmdTakeDamageMoviment(consumptionTranslation);
        }

        if (horizontal != 0 || vertical != 0)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, 0.0087f, transform.localPosition.z);
        }
    }

    [Command]
    void CmdTakeDamageMoviment(float damage)
    {
        health.TakeDamageGasoline(damage);
    }

    void OnDestroy()
    {
        GameObject joy = GameObject.FindGameObjectWithTag("JoystickControl");
        if (joy != null)
        {
            Destroy(joy);
        }
    }
    
    void OnCollisionEnter(Collision collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Casa":
                health.TakeDamageMaintenance(damageHouse);
                break;
            case "Mecanica":
                health.TakeDamageMaintenance(damageMechanics);
                break;
            case "PostoGasolina":
                health.TakeDamageMaintenance(damageGasStation);
                break;
        }
    }
    
    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Buraco")
        {
            health.TakeDamageMaintenance(damageHole);
        }
    }
    
    void OnTriggerStay(Collider collision)
    {
        switch (collision.gameObject.tag)
        {
            case "ChaveInglesa":
                if (!waitRepair)
                {
                    StartCoroutine(RepairMaintenance());
                }
                break;
            case "GalaoDeGasolina":
                if (!waitFuel)
                {
                    StartCoroutine(FuelGasoline());
                }
                break;
        }
    }

    private IEnumerator FuelGasoline()
    {
        health.TakeDamageGasoline(fuelForSecond);
        waitFuel = true;
        yield return new WaitForSeconds(1);
        waitFuel = false;
    }

    private IEnumerator RepairMaintenance()
    {
        health.TakeDamageMaintenance(repairForSecond);
        waitRepair = true;
        yield return new WaitForSeconds(1);
        waitRepair = false;
    }
}
