﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Multiplayer
{
    class SelectMultiplayerScript : MonoBehaviour
    {
        public void Voltar()
        {
            SceneManager.LoadScene("TelaInicial");
        }

        public void MultiplayerWeb()
        {
            SceneManager.LoadScene("Lobby");
        }

        public void MultiplayerLocalBluetooth()
        {
            SceneManager.LoadScene("LobbyBluetooth");
            //SceneManager.LoadScene("MultiplayerBluetooth");
        }

        public void MultiplayerLocalWifiDirect()
        {
            SceneManager.LoadScene("LobbyWifiDirect");
        }
    }
}
