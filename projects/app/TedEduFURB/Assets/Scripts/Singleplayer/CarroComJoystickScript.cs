﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class CarroComJoystickScript : MonoBehaviour
{
	private ControleBarrasScript Barras = null;
	private bool aguardarArrumarCarro = false;
	private bool aguardarEncherTanque = false;
	public float velocidadeTranslacao;
	public float velocidadeRotacao;
	public float perdaBarraGasTranslacao;
	public float perdaBarraGasRotacao;
	public float perdaBarraMecColisaoBuraco;
	public float perdaBarraMecColisaoCasa;
	public float perdaBarraMecColisaoPosto;
	public float perdaBarraMecColisaoMecanica;
	public float incrementoBarrraGasPorSegundo;
	public float incrementoBarraMecPorSegundo;

	// Use this for initialization
	void Start ()
	{
		this.Barras = FindObjectOfType<ControleBarrasScript> ();
	}

	// Update is called once per frame
	void Update ()
	{
		float movimentoHorizontal = CrossPlatformInputManager.GetAxis ("Horizontal");
		float movimentoVertical = CrossPlatformInputManager.GetAxis ("Vertical");

        // Rotaçao
        if (movimentoHorizontal != 0 && (movimentoHorizontal > 0.30f || movimentoHorizontal < -0.30f)) {
			float rotacao = (movimentoHorizontal * velocidadeRotacao) * Time.deltaTime;
			if (movimentoHorizontal < 0 && movimentoVertical < 0 ||
				movimentoHorizontal > 0 && movimentoVertical < 0f) {
				transform.Rotate (0, rotacao * -1, 0);
			} else {
				transform.Rotate (0, rotacao, 0);
			}

			Barras.DiminuiGasolinaAtual (perdaBarraGasRotacao);
		}
		
        //Translaçao
		if (movimentoVertical != 0) {
			float translacao = (movimentoVertical * velocidadeTranslacao) * Time.deltaTime;
			transform.Translate (0, 0, translacao);
			Barras.DiminuiGasolinaAtual (perdaBarraGasTranslacao);
		}
		
        //Mantem a posiçao y fixa devido a rotaçao movimentar o eixo y
		if (movimentoHorizontal != 0 || movimentoVertical != 0) {
			transform.localPosition = new Vector3 (transform.localPosition.x, 0.0087f, transform.localPosition.z);
		}

        if (Barras.gasolinaAtual <= 0) {
			Destroy (gameObject);
		}
	}

	void OnCollisionEnter (Collision ObjetoColidido)
	{
		switch (ObjetoColidido.gameObject.tag) {
		case "Casa":
			this.Barras.DiminuiMecanicaAtual (perdaBarraMecColisaoCasa);
			break;
		case "Mecanica":
			this.Barras.DiminuiMecanicaAtual (perdaBarraMecColisaoMecanica);
			break;
		case "PostoGasolina":
			this.Barras.DiminuiMecanicaAtual (perdaBarraMecColisaoPosto);
			break;
		}

		if (Barras.mecanicaAtual <= 0) {
			Destroy (gameObject);
		}
	}

	void OnDestroy() {
		GameObject joy = GameObject.FindGameObjectWithTag ("JoystickControl");
		if (joy != null) {
			Destroy(joy);
		}
	}

	void OnTriggerEnter (Collider ObjetoColidido)
	{
		if (ObjetoColidido.gameObject.tag == "Buraco") {
			this.Barras.DiminuiMecanicaAtual (perdaBarraMecColisaoBuraco);
		}
		if (Barras.mecanicaAtual <= 0) {
			Destroy (gameObject);
		}
	}

	void OnTriggerStay (Collider ObjetoColidido)
	{
		switch (ObjetoColidido.gameObject.tag) {
		case "ChaveInglesa":
			if (!this.aguardarArrumarCarro) {
				StartCoroutine (ArrumarCarro ());
			}
			break;
		case "GalaoDeGasolina":
			if (!this.aguardarEncherTanque) {
				StartCoroutine (EncherTanqueGasolina ());
			}
			break;
		}    
	}

	private IEnumerator EncherTanqueGasolina ()
	{
		this.Barras.IncrementaGasolinaAtual (incrementoBarrraGasPorSegundo);
		this.aguardarEncherTanque = true;
		yield return new WaitForSeconds (1);
		this.aguardarEncherTanque = false;
	}
	
	private IEnumerator ArrumarCarro ()
	{
		this.Barras.IncrementaMecanicaAtual (incrementoBarraMecPorSegundo);
		this.aguardarArrumarCarro = true;
		yield return new WaitForSeconds (1);
		this.aguardarArrumarCarro = false;
	}
}
