﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ControleBarrasScript : MonoBehaviour
{

	public float gasolinaAtual;
	public float gasolinaMaxima;
	public float mecanicaAtual;
	public float mecanicaMaxima;
	public Texture TexturaGasolina;
	public Texture TexturaMecanica;

	void OnGUI ()
	{ 
		// Desenha barra de gasolina
		GUI.BeginGroup (new Rect (0, 0, Screen.width / 2.0f, Screen.height / 10.0f));
		GUI.DrawTexture (new Rect (0, 0, (Screen.width / 2.0f) * gasolinaAtual / gasolinaMaxima, Screen.height / 10.0f), TexturaGasolina);
		GUI.Box (new Rect (0, 0, Screen.width / 2.0f, Screen.height / 10.0f), string.Format ("Gasolina"));
		GUI.EndGroup ();

		// Desenha barra de mecanica
		GUI.BeginGroup (new Rect (Screen.width / 2.0f, 0, Screen.width / 2.0f, Screen.height / 10.0f));
		GUI.DrawTexture (new Rect (0, 0, (Screen.width / 2.0f) * mecanicaAtual / mecanicaMaxima, Screen.height / 10.0f), TexturaMecanica);
		GUI.Box (new Rect (0, 0, Screen.width / 2.0f, Screen.height / 10.0f), string.Format ("Mecânica"));
		GUI.EndGroup ();

		// Quando acabar a gasolina ou a mecanica (Carro destruido)
		if (this.gasolinaAtual <= 0 || this.mecanicaAtual <= 0) {
			Destroy (GameObject.FindGameObjectWithTag ("BotaoVoltar"));
			
			float width = Screen.width / 4;
			float heigh = Screen.height / 2;
			float posX = Screen.width / 4 - width / 2;
			float posY = Screen.height / 2 - heigh / 2;
			if (GUI.Button (new Rect (posX, posY, width, heigh), "Recomeçar")) {
                SceneManager.LoadScene("ControlePorJoystick");
			}
			
			if (GUI.Button (new Rect (posX * 5, posY, width, heigh), "Tela inicial")) {
                SceneManager.LoadScene("TelaInicial");
			}
		}
	}

	public void DiminuiGasolinaAtual (float quantidade)
	{
		if (this.gasolinaAtual > 0) {
			this.gasolinaAtual = this.gasolinaAtual - quantidade;
		}
	}

	public void IncrementaGasolinaAtual (float quantidade)
	{
		if (gasolinaAtual < gasolinaMaxima) {
			this.gasolinaAtual = this.gasolinaAtual + quantidade;
		}
	}

	public void DiminuiMecanicaAtual (float quantidade)
	{
		if (this.mecanicaAtual > 0) {
			this.mecanicaAtual = this.mecanicaAtual - quantidade;
		}
	}
	
	public void IncrementaMecanicaAtual (float quantidade)
	{
		if (this.mecanicaAtual < this.mecanicaMaxima) {
			this.mecanicaAtual = this.mecanicaAtual + quantidade;
		}
	}
}
