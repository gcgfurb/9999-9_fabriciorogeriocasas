package com.furb.tecedu.unitypluginbluetooth;

import java.util.ArrayList;

public interface IService {

    ArrayList<String> getBondedDevices();

    void start(String identifier);

    void pause();

    void resume();

    void stop();

    void read();

    boolean write(byte[] data);
}
