package com.furb.tecedu.unitypluginbluetooth;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.util.Log;

import com.unity3d.player.UnityPlayer;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class Service implements ServiceListener {

    private static Service instance = new Service();
    private static IService service = null;

    private static String gameObject = IConstants.TAG;

    public static void createServiceClient(String gameObjectName) {
        Log.i(IConstants.TAG, "(Service) createServiceClient");
        if (service == null) {
            final Activity activity = UnityPlayer.currentActivity;
            if (!activity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH)) {
                return;
            }
            service = new ServiceClient(activity);

            if(instance == null)Log.i(IConstants.TAG, "(Service) instancia nulo");
            ((ServiceBase) service).addListener(instance);
        }

        gameObject = gameObjectName;
    }

    public static void createServiceServer(String gameObjectName) {
        Log.i(IConstants.TAG, "(Service) createServiceServer");
        if (service == null) {
            final Activity activity = UnityPlayer.currentActivity;
            if (!activity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH)) {
                return;
            }
            service = new ServiceServer(activity);
            ((ServiceBase) service).addListener(instance);
        }

        gameObject = gameObjectName;
    }

    public static String[] getBondedDevices() {
        if (service != null) {
            ArrayList<String> array = service.getBondedDevices();
            if (array != null) {
                String[] devices = new String[array.size()];
                return array.toArray(devices);
            }
        }
        return null;
    }

    public static void start(String uuidString) {
        if (service != null) {
            service.start(uuidString);
            Log.i(IConstants.TAG, "(Service) start");
        }
    }

    public static void pause() {
        if (service != null) {
            service.pause();
            Log.i(IConstants.TAG, "(Service) pause");
        }
    }

    public static void stop() {
        if (service != null) {
            service.stop();
            Log.i(IConstants.TAG, "(Service) stop");
        }
    }

    public static void write(byte[] data) {
        if (service != null) {
            service.write(data);
            Log.i(IConstants.TAG, "(Service) write: " + new String(data, StandardCharsets.UTF_8));
        } else {
            Log.i(IConstants.TAG, "(Service) write: service nulo");
        }
    }

    @Override
    public void onDeviceConnect(boolean connect) {
        if (connect) {
            UnityPlayer.UnitySendMessage(gameObject, "OnDidConnect", "onDeviceConnect true");
            Log.i(IConstants.TAG, "(Service) OnDidConnect");
        } else {
            UnityPlayer.UnitySendMessage(gameObject, "OnDidDisconnect", "onDeviceConnect false");
            Log.i(IConstants.TAG, "(Service) OnDidDisconnect: " + gameObject);
        }
    }

    @Override
    public void onSocketRead(final byte[] bytes) {
        String encoded = ServiceUtil.encodeBase64(bytes);
        UnityPlayer.UnitySendMessage(gameObject, "OnDidReceiveWriteRequests", encoded);
        Log.i(IConstants.TAG, "(Service) OnDidReceiveWriteRequests");

    }

    @Override
    public void onSocketWrite(byte[] bytes) {
        String msg = new String(bytes);
        Log.i(IConstants.TAG, "(Service) onSocketWrite: " + msg);
    }
}
