package com.furb.tecedu.unitypluginbluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

public class ServiceClient extends ServiceBase implements IService {

    private BroadcastReceiver mReceiver = null;
    private ConnectThread mConnectThread = null;
    private ConnectedThread mConnectedThread = null;

    public ServiceClient(final Activity activity) {
        super(activity);
    }

    @Override
    public ArrayList<String> getBondedDevices() {
        Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();

        ArrayList<String> list = null;
        // If there are paired devices
        if (pairedDevices.size() > 0) {
            list = new ArrayList<String>();
            // Loop through paired devices
            for (BluetoothDevice device : pairedDevices) {
                // Add the name and address to an array adapter to show in a ListView
                list.add(device.getName() + "<>" + device.getAddress());
            }
        }

        return list;
    }

    @Override
    public void start(String pairingAddress) {
        mPairingAddress = pairingAddress;
        scanDevice();
    }

    @Override
    public void pause() {
        if (mReceiver != null) {
            mActivity.unregisterReceiver(mReceiver);
        }
        if (mConnectedThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }
        if (mConnectedThread !=  null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }
        for (ServiceListener listener : listeners) {
            if(listener != null) {
                listener.onDeviceConnect(false);
            } else {
                Log.i(IConstants.TAG, "(ServiceClient) pause: listner nulo / count " + listeners.size());
            }
        }
    }

    @Override
    public void resume() {
        scanDevice();
    }

    @Override
    public void stop() {
        pause();
    }

    @Override
    public void read() {

    }

    @Override
    public boolean write(byte[] data) {
        if (mConnectedThread != null) {
            mConnectedThread.write(data);
            Log.i(IConstants.TAG, "(ServiceClient) write: " + new String(data, StandardCharsets.UTF_8));
            return true;
        }

        Log.i(IConstants.TAG, "(ServiceClient) write: thread nula");
        return false;
    }

    private void scanDevice() {
        for (BluetoothDevice device : mPairedDevices) {
            if (mPairingAddress.equals(device.getAddress())) {
                mConnectThread = new ConnectThread(device);
                mConnectThread.start();
                for (ServiceListener listener : listeners) {
                    if(listener != null) {
                        Log.i(IConstants.TAG, "(ServiceClient) scanDevice: listner ok");
                        listener.onDeviceConnect(true);
                    } else {
                        Log.i(IConstants.TAG, "(ServiceClient) scanDevice: listner nulo");
                    }

                }
                return;
            }
        }
        mReceiver = createBroadcastReceiver(mPairingAddress);
        if (mBtAdapter.isDiscovering()) {
            mBtAdapter.cancelDiscovery();
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        mActivity.registerReceiver(mReceiver, filter);
        mBtAdapter.startDiscovery();
    }

    private BroadcastReceiver createBroadcastReceiver(final String pairingAddress) {
        return new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                        if (pairingAddress.equals(device.getAddress())) {
                            mConnectThread = new ConnectThread(device);
                            mConnectThread.start();
                            for (ServiceListener listener : listeners) {
                                listener.onDeviceConnect(true);
                            }
                        }
                    }
                }
            }
        };
    }

    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        protected ConnectThread(BluetoothDevice device) {
            BluetoothSocket tmp = null;
            mmDevice = device;

            try {
                tmp = device.createRfcommSocketToServiceRecord(UUID.fromString(CHARACTERISTIC_UUID));
            } catch (IOException e) {
                Log.e(IConstants.TAG, "(ServiceClient) ConnectThread: "+e.getMessage());
            }
            mmSocket = tmp;
        }

        @Override
        public void run() {
            if (mBtAdapter.isDiscovering()) {
                mBtAdapter.cancelDiscovery();
            }
            try {
                mmSocket.connect();
            } catch (IOException connectException) {
                try {
                    mmSocket.close();
                } catch (IOException e) {
                    Log.e(IConstants.TAG, "(ServiceClient) ConnectThread - cancel: "+e.getMessage());
                    return;
                }
            }
            Log.i(IConstants.TAG, "(ServiceClient) ConnectThread - run: Inicia thread connected");
            mConnectedThread = new ConnectedThread(mmSocket);
            mConnectedThread.start();
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(IConstants.TAG, "(ServiceClient) ConnectThread - cancel: "+e.getMessage());
            }
        }
    }
}