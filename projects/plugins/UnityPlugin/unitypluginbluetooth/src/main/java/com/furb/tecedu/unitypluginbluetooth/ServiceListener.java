package com.furb.tecedu.unitypluginbluetooth;

public interface ServiceListener {

    void onDeviceConnect(boolean connect);

    void onSocketRead(final byte[] bytes);

    void onSocketWrite(final byte[] bytes);
}
