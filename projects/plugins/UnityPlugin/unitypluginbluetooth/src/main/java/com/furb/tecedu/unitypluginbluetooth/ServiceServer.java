package com.furb.tecedu.unitypluginbluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.util.Log;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

public class ServiceServer extends ServiceBase implements IService {

    private AcceptThread mAcceptThread = null;
    private ConnectedThread mConnectedThread = null;

    public ServiceServer(final Activity activity) {
        super(activity);
    }

    @Override
    public ArrayList<String> getBondedDevices() {

        Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();

        ArrayList<String> list = null;
        // If there are paired devices
        if (pairedDevices.size() > 0) {
            list = new ArrayList<String>();
            // Loop through paired devices
            for (BluetoothDevice device : pairedDevices) {
                // Add the name and address to an array adapter to show in a ListView
                list.add(device.getName() + "<>" + device.getAddress());
            }
        }

        return list;
    }

    @Override
    public void start(String pairingAddress) {
        mPairingAddress = pairingAddress;
        discoverableDevice();
    }

    @Override
    public void pause() {
        if (mAcceptThread != null) {
            mAcceptThread.cancel();
            mAcceptThread = null;
        }
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }
        for (ServiceListener listener : listeners) {
            if(listener != null) {
                listener.onDeviceConnect(false);
            } else {
                Log.i(IConstants.TAG, "(ServiceServer) pause: listner nulo");
            }
        }
    }

    @Override
    public void resume() {
        discoverableDevice();
    }

    @Override
    public void stop() {
        pause();
    }

    @Override
    public void read() {

    }

    @Override
    public boolean write(byte[] data) {
        if (mConnectedThread != null) {
            mConnectedThread.write(data);
            Log.i(IConstants.TAG, "(ServiceServer) write: " + new String(data, StandardCharsets.UTF_8));
            return true;
        }
        Log.i(IConstants.TAG, "(ServiceServer) write: thread nula");
        return false;
    }

    private void discoverableDevice() {

        Log.i(IConstants.TAG, "(ServiceServer) discoverableDevice: " + mPairingAddress);
        boolean bonded = false;
        for (BluetoothDevice device : mPairedDevices) {
            if (mPairingAddress.equals(device.getAddress())) {
                bonded = true;
                break;
            }
        }

        if (!bonded) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            mActivity.startActivity(discoverableIntent);
        }

        mAcceptThread = new AcceptThread();
        mAcceptThread.start();
    }

    private class AcceptThread extends Thread {
        private final BluetoothServerSocket mmServerSocket;

        private AcceptThread() {
            BluetoothServerSocket tmp = null;
            try {
                tmp = mBtAdapter.listenUsingRfcommWithServiceRecord("_", UUID.fromString(CHARACTERISTIC_UUID));
            } catch (IOException e) {
                Log.e(IConstants.TAG, "(ServiceServer) AcceptThread: "+e.getMessage());
            }
            mmServerSocket = tmp;
        }

        public void run() {
            BluetoothSocket socket = null;
            while (true) {
                try {
                    socket = mmServerSocket.accept();
                } catch (IOException e) {
                    break;
                }

                if (socket != null) {
                    mConnectedThread = new ConnectedThread(socket);
                    mConnectedThread.start();
                    for (ServiceListener listener : listeners) {
                        listener.onDeviceConnect(true);
                    }
                    try {
                        mmServerSocket.close();
                    } catch (IOException e) {
                        Log.e(IConstants.TAG, "(ServiceServer) AcceptThread - run: "+e.getMessage());
                    }
                    break;
                }
            }
        }

        public void cancel() {
            try {
                mmServerSocket.close();
            } catch (IOException e) {
                Log.e(IConstants.TAG, "(ServiceServer) AcceptThread - cancel: "+e.getMessage());
            }
        }
    }
}
